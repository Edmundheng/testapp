//
//  MovieHomeViewController.h
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import <UIKit/UIKit.h>
#import "genreCollectionCell.h"
#import "TMDBCollectionViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface MovieHomeViewController : UIViewController <UISearchBarDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, genreCollectionTouchDelegate, movieTouchDelegate>

- (void) startSearchForGenre:(int)genreID;
-(void) startSearch:(NSString *)searchString;

@end

NS_ASSUME_NONNULL_END
