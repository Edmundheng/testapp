//
//  AppDelegate.h
//  testApp
//
//  Created by Edmund Heng on 30/11/20.
//

#import <UIKit/UIKit.h>
#import "TMDBConfiguration.h"
#import "TMDBGenreListRequest.h"

#define genresUpdatedNotification @"genresUpdated"
#define movieListUpdatedNotification @"movieListUpdated"
#define configurationKey @"TMDBConfiguration"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) TMDBConfiguration *configuration;
@property (strong, nonatomic) NSArray *genreList;
@property (strong, nonatomic) NSMutableDictionary *movieList;
@property (assign, nonatomic) int currentGenre;

- (void)updateMovieList:(NSDictionary *)movieList currentPage:(int)page andGenre:(int)genre;
- (void) getMoreMovies:(int)pageToRetrieve;
- (void) startSearch:(NSString *)searchText;
- (void) clearSearchMode;

@end

