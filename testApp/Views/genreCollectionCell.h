//
//  genreCollectionCell.h
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class genreCollectionCell;

@protocol genreCollectionTouchDelegate <NSObject>

- (void)searchForGenre:(genreCollectionCell *)genreCell;

@end

@interface genreCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) id<genreCollectionTouchDelegate>delegate;

- (void) updateState:(BOOL) selected;
-(void) updateButton:(NSDictionary *)genre;
@end

NS_ASSUME_NONNULL_END
