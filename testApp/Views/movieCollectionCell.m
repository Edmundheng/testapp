//
//  movieCollectionCell.m
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import "movieCollectionCell.h"


@implementation movieCollectionCell

-(void) updateWithMovie:(TMDBMovie *)movie{
    UIImage *placeholder = [UIImage imageNamed:@"icon_placeholder"];
    [self.imageView sd_setImageWithURL:movie.posterImageURL placeholderImage:placeholder completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
    }];
    
    self.title.text = movie.title;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"YYYY-MM-DD";
    NSDate *date = [dateFormatter dateFromString:movie.releaseDate];
    dateFormatter.dateFormat = @"YYYY";
    
    self.year.text = [dateFormatter stringFromDate:date];
}

-(void)awakeFromNib {
    [super awakeFromNib];
    self.cornerView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.cornerView.layer.shadowOffset = CGSizeMake(0.0f, -3.0f);
    self.cornerView.layer.shadowOpacity = 0.4f;
    self.cornerView.layer.cornerRadius = 5;
    self.cornerView.layer.borderColor = UIColor.grayColor.CGColor;
    self.cornerView.layer.borderWidth = 1.0f;
}

@end
