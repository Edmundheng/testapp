//
//  UILoadingView.m
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import "UILoadingView.h"

@implementation UILoadingView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)layoutSubviews {
    //only apply the blur if the user hasn't disabled transparency effects
    [super layoutSubviews];
    
    if (!UIAccessibilityIsReduceTransparencyEnabled() && !self.blurEffectView) {
        self.backgroundColor = [UIColor clearColor];

        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleRegular];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        //always fill the view
        blurEffectView.frame = self.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

        [self addSubview:blurEffectView]; //if you have more UIViews, use an insertSubview API to place it where needed
        self.blurEffectView = blurEffectView;
    } 
    
    if (!self.indicator) {
        self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleMedium];
        self.indicator.center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
        [self addSubview:self.indicator];
        [self.indicator startAnimating];
    }
}

@end
