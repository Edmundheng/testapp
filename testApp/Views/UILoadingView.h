//
//  UILoadingView.h
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILoadingView : UIView

@property (strong, nonatomic) UIVisualEffectView *blurEffectView;
@property (strong, nonatomic) UIActivityIndicatorView *indicator;

@end

NS_ASSUME_NONNULL_END
