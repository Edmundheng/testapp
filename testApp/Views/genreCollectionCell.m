//
//  genreCollectionCell.m
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import "genreCollectionCell.h"


@implementation genreCollectionCell
-(void) updateButton:(NSDictionary *)genre {
    NSNumber *number = [genre objectForKey:@"id"];
    NSString *text = [genre objectForKey:@"name"];
    [self.button setTitle:text forState:UIControlStateNormal];
    self.tag = number.intValue;
}

- (void) updateState:(BOOL) selected{
    if (selected) {
        [self.button.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0f]];
    } else {
        [self.button.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    }
}

- (IBAction)genreTouched:(UIButton *)sender {
    [self updateState:YES];
    if (self.delegate) {
        [self.delegate searchForGenre:self];
    }
}

@end
