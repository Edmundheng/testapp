//
//  movieCollectionCell.h
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import <UIKit/UIKit.h>
#import "TMDBMovie.h"
#import <SDWebImage/SDWebImage.h>

NS_ASSUME_NONNULL_BEGIN

@class movieCollectionCell;

@interface movieCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *year;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *cornerView;

-(void) updateWithMovie:(TMDBMovie *)movie;
@end

NS_ASSUME_NONNULL_END
