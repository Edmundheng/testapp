//
//  TMDBHelper.m
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import "TMDBHelper.h"

#import "TMDBConfigurationRequest.h"
#import "TMDBGenreListRequest.h"
#import "TMDBDiscoverRequest.h"
#import "TMDBMovieDetailRequest.h"
#import "TMDBMoviePreviewRequest.h"
#import "RequestHelper.h"

@implementation TMDBHelper

+(void) getConfiguration:(TMDBHandler) handler {
    TMDBConfigurationRequest *urlReq = [TMDBConfigurationRequest createRequest:handler];
    [RequestHelper startRequest:urlReq];
}

+(void) getGenreList:(TMDBHandler)handler {
    TMDBGenreListRequest *urlReq = [TMDBGenreListRequest createRequest:handler];
    [RequestHelper startRequest:urlReq];
}

+(void) searchForGenre:(int)genre handler:(TMDBHandler)handler {
    TMDBDiscoverRequest *urlReq = [TMDBDiscoverRequest createRequest:genre completion:handler];
    [RequestHelper startRequest:urlReq];
}

+(void) searchForGenre:(int)genre page:(int)page handler:(TMDBHandler)handler {
    TMDBDiscoverRequest *urlReq = [TMDBDiscoverRequest createRequest:genre page:page completion:handler];
    [RequestHelper startRequest:urlReq];
}

+(void) getDetailsForMovie:(TMDBMovie *)movie handler:(TMDBHandler)handler {
    TMDBMovieDetailRequest *urlReq = [TMDBMovieDetailRequest createRequest:movie completion:handler];
    [RequestHelper startRequest:urlReq];
}

+(void) getPreviewsForMovie:(TMDBMovie *)movie handler:(TMDBHandler)handler {
    TMDBMoviePreviewRequest *urlReq = [TMDBMoviePreviewRequest createRequest:movie completion:handler];
    [RequestHelper startRequest:urlReq];
}

@end
