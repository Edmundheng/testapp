//
//  TMDBGenreListRequest.m
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import "TMDBGenreListRequest.h"

#define URL_METHOD @"genre/movie/list"

@implementation TMDBGenreListRequest

+ (instancetype) createRequest:(TMDBHandler)completion {
    TMDBGenreListRequest *myRequest = [[TMDBGenreListRequest alloc] initWithGetRequest:URL_METHOD params:nil completion:completion];
    return myRequest;
}

- (void) processNetworkResponse {
    NSDictionary *d = [self dictionaryOrErrorFromRequest];
    if([d isKindOfClass:[NSError class]])
    {
        safe_block(self.completion)(nil, (NSError*)d);
        return;
    }
    NSDictionary *results = [d objectForKey:@"genres"];
//    TMDBConfiguration *configuration = [[TMDBConfiguration alloc] initWithDictionary:d];

    safe_block(self.completion)(results, nil);
}



@end
