//
//  TMDBMovieDetailRequest.h
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import <Foundation/Foundation.h>
#import "TMDBRequest.h"
#import "TMDBConfiguration.h"
#import "TMDBMovie.h"

NS_ASSUME_NONNULL_BEGIN

@interface TMDBMovieDetailRequest : TMDBRequest

@property (nonatomic, strong) TMDBMovie *movie;

+ (TMDBMovieDetailRequest *) createRequest:(TMDBMovie *)movie completion:(TMDBHandler)completion;

@end

NS_ASSUME_NONNULL_END
