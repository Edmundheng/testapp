//
//  TMDBHelper.h
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import <Foundation/Foundation.h>
#import "TMDBRequest.h"

@class TMDBMovie;

NS_ASSUME_NONNULL_BEGIN

@interface TMDBHelper : NSObject

+(void) getConfiguration:(TMDBHandler) handler;
+(void) getGenreList:(TMDBHandler) handler;
+(void) searchForGenre:(int)genre handler:(TMDBHandler)handler;
+(void) searchForGenre:(int)genre page:(int)page handler:(TMDBHandler)handler;

+(void) getDetailsForMovie:(TMDBMovie *)movie handler:(TMDBHandler)handler;
+(void) getPreviewsForMovie:(TMDBMovie *)movie handler:(TMDBHandler)handler;

@end

NS_ASSUME_NONNULL_END
