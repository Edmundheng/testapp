//
//  TMDBGenreListRequest.h
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import <Foundation/Foundation.h>
#import "TMDBRequest.h"
#import "TMDBConfiguration.h"

NS_ASSUME_NONNULL_BEGIN

@interface TMDBGenreListRequest : TMDBRequest

+ (TMDBGenreListRequest *) createRequest:(TMDBHandler)completion;

@end

NS_ASSUME_NONNULL_END
