//
//  TMDBMoviePreviewRequest.m
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import "TMDBMoviePreviewRequest.h"
#import "AppDelegate.h"

#define URL_METHOD @"movie/"
#define URL_ENDMETHOD @"/videos"

@implementation TMDBMoviePreviewRequest

+ (TMDBMoviePreviewRequest *) createRequest:(TMDBMovie *)movie completion:(TMDBHandler)completion {
//    NSMutableDictionary *dict = [NSMutableDictionary new];
    NSString *finalURLMethod = [NSString stringWithFormat:@"%@%i%@",URL_METHOD,movie.movieID,URL_ENDMETHOD];
//
//    [dict setObject:@"release_date.desc" forKey:@"sort_by"];
//    [dict setObject:@"false" forKey:@"include_adult"];
//    [dict setObject:@"true" forKey:@"include_video"];
    
    TMDBMoviePreviewRequest *myRequest = [[TMDBMoviePreviewRequest alloc] initWithGetRequest:finalURLMethod params:nil completion:completion];
    myRequest.movie = movie;
//    myRequest.genreToDiscover = genre;
    return myRequest;
}

- (void) processNetworkResponse {
    NSDictionary *d = [self dictionaryOrErrorFromRequest];
    if([d isKindOfClass:[NSError class]])
    {
        safe_block(self.completion)(nil, (NSError*)d);
        return;
    }
    
    NSArray *results = [d objectForKey:@"results"];
    NSMutableArray *array = [NSMutableArray new];
    for (NSDictionary *dict in results) {
        TMDBMoviePreview *preview = [[TMDBMoviePreview alloc] initWithDictionary:dict];
        [array addObject:preview];
    }
    
    self.movie.previews = array;
//    TMDBConfiguration *configuration = [[TMDBConfiguration alloc] initWithDictionary:d];

    safe_block(self.completion)(self.movie, nil);
}



@end
