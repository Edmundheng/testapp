//
//  RequestHelper.h
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import <Foundation/Foundation.h>

@class TMDBRequest;

NS_ASSUME_NONNULL_BEGIN

@interface RequestHelper : NSObject

+ (NSString *)getURLString:(NSString *)urlString parameters:(NSDictionary *)parameters;
+ (void) startRequest:(TMDBRequest *)request;

@end

NS_ASSUME_NONNULL_END
