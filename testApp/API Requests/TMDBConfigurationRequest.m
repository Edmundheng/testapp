//
//  TMDBConfigurationRequest.m
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import "TMDBConfigurationRequest.h"

#define URL_METHOD @"configuration"

@implementation TMDBConfigurationRequest

+ (instancetype) createRequest:(TMDBHandler)completion {
    TMDBConfigurationRequest *myRequest = [[TMDBConfigurationRequest alloc] initWithGetRequest:URL_METHOD params:nil completion:completion];
//    myRequest.networkHandler = ^(id item, NSError * _Nullable error) {
//        TMDBConfigurationRequest *mRequest = item;
//        [mRequest processNetworkResponseForConfiguration];
//    };
    return myRequest;
}

- (void) processNetworkResponse {
    NSDictionary *d = [self dictionaryOrErrorFromRequest];
    if([d isKindOfClass:[NSError class]])
    {
        safe_block(self.completion)(nil, (NSError*)d);
        return;
    }
    
    TMDBConfiguration *configuration = [[TMDBConfiguration alloc] initWithDictionary:d];

    safe_block(self.completion)(configuration, nil);
}



@end
