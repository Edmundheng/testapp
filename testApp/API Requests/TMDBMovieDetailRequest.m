//
//  TMDBMovieDetailRequest.m
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import "TMDBMovieDetailRequest.h"
#import "AppDelegate.h"
#import "TMDBMovie.h"

#define URL_METHOD @"movie/"

@implementation TMDBMovieDetailRequest

+ (TMDBMovieDetailRequest *) createRequest:(TMDBMovie *)movie completion:(TMDBHandler)completion {
//    NSMutableDictionary *dict = [NSMutableDictionary new];
    NSString *finalURLMethod = [NSString stringWithFormat:@"%@%i",URL_METHOD,movie.movieID];
//
//    [dict setObject:@"release_date.desc" forKey:@"sort_by"];
//    [dict setObject:@"false" forKey:@"include_adult"];
//    [dict setObject:@"true" forKey:@"include_video"];
    
    TMDBMovieDetailRequest *myRequest = [[TMDBMovieDetailRequest alloc] initWithGetRequest:finalURLMethod params:nil completion:completion];
    myRequest.movie = movie;
//    myRequest.genreToDiscover = genre;
    return myRequest;
}

- (void) processNetworkResponse {
    NSDictionary *d = [self dictionaryOrErrorFromRequest];
    if([d isKindOfClass:[NSError class]])
    {
        safe_block(self.completion)(nil, (NSError*)d);
        return;
    }
    
    [self.movie populateDetails:d];

    safe_block(self.completion)(self.movie, nil);
}



@end
