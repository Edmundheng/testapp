//
//  RequestHelper.m
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import "RequestHelper.h"
#import "TMDBRequest.h"

@implementation RequestHelper

#pragma mark URL connections

+ (void) startRequest:(TMDBRequest *)request {
    [[[NSURLSession sharedSession] dataTaskWithRequest:request.request completionHandler:^(NSData *data, NSURLResponse *response, NSError *connectionError){
        
        request.responseData = data;
        request.error = connectionError;
        request.responseHeaders = [(NSHTTPURLResponse*)response allHeaderFields];

        dispatch_sync(dispatch_get_main_queue(), ^{
            safe_block(request.networkHandler)(request, connectionError);
        });
    }] resume];
}

+ (void) endRequest:(TMDBRequest *)request {
    
}

#pragma mark URL String creator for GET requests

+ (NSString *)getURLString:(NSString *)urlString parameters:(NSDictionary *)parameters
{
    urlString = [self sanatizeSlashesFromURLString:urlString];
    
    NSString *urlStringParameters = [RequestHelper urlRequestStringForParameters:parameters];
    
    if (urlStringParameters.length)
    {
        urlString = [NSString stringWithFormat:@"%@?%@", urlString, urlStringParameters];
    }
    else
    {
        urlString = urlString;
    }
    
    return urlString;
}

+ (NSString *)sanatizeSlashesFromURLString:(NSString *)urlString
{
    //Double slashes in URL cause problems in the backend.  Do some stunt-work to get rid of them.
    NSMutableString *urlMutableString = urlString.mutableCopy;
    [urlMutableString replaceOccurrencesOfString:@"//" withString:@"/" options:NSCaseInsensitiveSearch range:NSMakeRange(0, urlMutableString.length)];
    [urlMutableString replaceOccurrencesOfString:@"http:/" withString:@"http://" options:NSCaseInsensitiveSearch range:NSMakeRange(0, urlMutableString.length)];
    [urlMutableString replaceOccurrencesOfString:@"https:/" withString:@"https://" options:NSCaseInsensitiveSearch range:NSMakeRange(0, urlMutableString.length)];

    NSString *urlFinalString = [NSString stringWithString:urlMutableString];
    
    return urlFinalString;
}

+ (NSString *)urlRequestStringForParameters:(NSDictionary *)params
{
    NSMutableArray *pairs = [NSMutableArray array];
    for (NSString *key in [params keyEnumerator])
    {
        id value = params[key];
        if ([value isKindOfClass:[NSDictionary class]]) {
            
            for (NSString *subKey in value)
            {
                [pairs addObject:[NSString stringWithFormat:@"%@[%@]=%@", key, subKey, [value[subKey] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]]];
            }
        } else if ([value isKindOfClass:[NSArray class]]) {
            
            NSMutableString *subValueString = [[NSMutableString alloc] init];
            
            [value enumerateObjectsUsingBlock:^(id subValue, NSUInteger idx, BOOL *stop) {
                if ([subValue isKindOfClass:[NSString class]]) {
                    [subValueString appendFormat:@"%@",[subValue stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
                } else {
                    [subValueString appendFormat:@"%@",[subValue stringValue]];
                }
                
                if (idx < [value count] - 1) {
                    [subValueString appendFormat:@","];
                }
            }];

            [pairs addObject:[NSString stringWithFormat:@"%@=[%@]",key, subValueString]];
        }
        else if ([value isKindOfClass:[NSString class]])
        {
            value = [value stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
            value = [value stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
            [pairs addObject:[NSString stringWithFormat:@"%@=%@", key, value]];
        }
        else
        {
            [pairs addObject:[NSString stringWithFormat:@"%@=%@", key, value]];
        }
    }
    
    NSArray *sortedArray = [pairs sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    return [sortedArray componentsJoinedByString:@"&"];
}

@end
