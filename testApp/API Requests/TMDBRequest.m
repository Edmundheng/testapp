//
//  TMDBRequest.m
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import "TMDBRequest.h"
#import "RequestHelper.h"

#define API_KEY_STRING @"api_key"

#define TMDB_TIMEOUT [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"ApplicationConfiguration"] objectForKey:@"TMDB_TIMEOUT"];
#define TMDB_API_KEY  [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"ApplicationConfiguration"] objectForKey:@"TMDB_API_KEY"];
#define TMDB_DOMAIN  [[[NSBundle mainBundle] objectForInfoDictionaryKey:@"ApplicationConfiguration"] objectForKey:@"TMDB_DOMAIN"];

@implementation TMDBRequest

- (instancetype) initWithGetRequest:(NSString *)method params:(NSDictionary *)bodyParams completion:(TMDBHandler)completion {
    
    self = [super init];
    if (self)
    {
        NSMutableURLRequest *request = [NSMutableURLRequest new];
        NSString *baseURL = TMDB_DOMAIN;
        if (!baseURL || !baseURL.length) {
            return nil;
        }
        NSString *urlString = [NSString stringWithFormat:@"%@/%@",baseURL,method];
        
        NSMutableDictionary *body = [NSMutableDictionary dictionaryWithDictionary:bodyParams];
        NSString *apiKey = TMDB_API_KEY;
        [body setObject:apiKey forKey:API_KEY_STRING];
        
        urlString = [RequestHelper getURLString:urlString parameters:body];
        NSNumber *timeout = TMDB_TIMEOUT;
        int timeoutInterval = timeout ? timeout.intValue : 30;
        
        if (timeoutInterval != 0)
        {
            request.timeoutInterval = (NSTimeInterval)timeoutInterval;
        }
        request.URL = [NSURL URLWithString:urlString];
        request.HTTPMethod = @"GET";
        self.request = request;
        self.completion = completion;
    //    request.HTTPBody = [request.body dataUsingEncoding:NSUTF8StringEncoding];
    //    request.allHTTPHeaderFields = request.headers;
    //    request.completed = NO;
        
        self.networkHandler = ^(id item, NSError * _Nullable error) {
            [item processNetworkResponse];
        };
    }
    return self;
}

- (void) processNetworkResponse {
    return;
}

- (id)dictionaryOrErrorFromRequest {
    if (self.error)
    {
        return self.error;
    }
    
    NSError *error;
    NSDictionary *d = nil;
    if (self.responseData != nil)
    {
        d = (NSDictionary*)[NSJSONSerialization JSONObjectWithData:self.responseData options:0 error:&error];
        
        if (error)
        {
            NSLog(@"Error parsing json for request: %@\n%@", self.request.URL.absoluteString, error);
            error = [NSError errorWithDomain:@"Json Data Error"
                                        code:0
                                    userInfo:@{
                                               NSLocalizedDescriptionKey:
                                                   @"Malformed data was encountred."}];
        }
    }
    else
    {
        error = [NSError errorWithDomain:@"Network"
                                    code:0
                                userInfo:@{@"User Info":@"No Data"}];
        return error;
    }
    
    if(error != nil)
    {
        return error;
    }
    
//    error = [MCDMiddlewareCustomerSecurityError errorForNetworkResponseJson:d];
//    if (error)
//    {
//        return error;
//    }
    
    return d;
}

@end
