//
//  TMDBRequest.h
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import <Foundation/Foundation.h>


#define safe_block(_block) if(_block) _block

//Standard
typedef void (^DefaultHandler) (BOOL success, NSError * _Nullable error);


@interface TMDBRequest : NSObject

typedef void (^TMDBHandler) (id item, NSError * _Nullable error);

@property (strong, nonatomic) TMDBHandler completion;
@property (strong, nonatomic) TMDBHandler networkHandler;
@property (strong, nonatomic) NSMutableURLRequest *request;

@property (nonatomic, strong) NSData *responseData;
@property (nonatomic, strong) NSError *error;
@property (nonatomic,strong) NSDictionary *responseHeaders;

- (instancetype) initWithGetRequest:(NSString *)method params:(NSDictionary *)bodyParams completion:(TMDBHandler)completion;
- (void) processNetworkResponse;
- (id) dictionaryOrErrorFromRequest;

@end

