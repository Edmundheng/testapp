//
//  TMDBDiscoverRequest.h
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import <Foundation/Foundation.h>
#import "TMDBRequest.h"
#import "TMDBConfiguration.h"

NS_ASSUME_NONNULL_BEGIN

@interface TMDBDiscoverRequest : TMDBRequest

@property (nonatomic, assign) int genreToDiscover;

+ (TMDBDiscoverRequest *) createRequest:(int)genre completion:(TMDBHandler)completion;
+ (TMDBDiscoverRequest *) createRequest:(int)genre page:(int)page completion:(TMDBHandler)completion;

@end

NS_ASSUME_NONNULL_END
