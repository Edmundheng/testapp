//
//  TMDBDiscoverRequest.m
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import "TMDBDiscoverRequest.h"
#import "TMDBMovie.h"
#import "AppDelegate.h"

#define URL_METHOD @"discover/movie"

@implementation TMDBDiscoverRequest

+ (TMDBDiscoverRequest *) createRequest:(int)genre completion:(TMDBHandler)completion {
    NSMutableDictionary *dict = [NSMutableDictionary new];
    if (genre != -1)
        [dict setObject:@(genre) forKey:@"with_genres"];
    [dict setObject:@"release_date.desc" forKey:@"sort_by"];
    [dict setObject:@"false" forKey:@"include_adult"];
    [dict setObject:@"true" forKey:@"include_video"];
    
    TMDBDiscoverRequest *myRequest = [[TMDBDiscoverRequest alloc] initWithGetRequest:URL_METHOD params:dict completion:completion];
    myRequest.genreToDiscover = genre;
    return myRequest;
}

+ (TMDBDiscoverRequest *) createRequest:(int)genre page:(int)page completion:(TMDBHandler)completion {
    NSMutableDictionary *dict = [NSMutableDictionary new];
    if (genre != -1)
        [dict setObject:@(genre) forKey:@"with_genres"];
    [dict setObject:@(page) forKey:@"page"];
    [dict setObject:@"release_date.desc" forKey:@"sort_by"];
    [dict setObject:@"false" forKey:@"include_adult"];
    [dict setObject:@"true" forKey:@"include_video"];
    
    TMDBDiscoverRequest *myRequest = [[TMDBDiscoverRequest alloc] initWithGetRequest:URL_METHOD params:dict completion:completion];
    myRequest.genreToDiscover = genre;
    return myRequest;
}

- (void) processNetworkResponse {
    NSDictionary *d = [self dictionaryOrErrorFromRequest];
    if([d isKindOfClass:[NSError class]])
    {
        safe_block(self.completion)(nil, (NSError*)d);
        return;
    }
    
    NSMutableDictionary *pageItem = [NSMutableDictionary new];
    NSArray *results = [d objectForKey:@"results"];
    NSMutableArray *result_converted = [NSMutableArray new];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    TMDBConfiguration *config = appDelegate.configuration;
    
    if (!config) {
        safe_block(self.completion)(nil, [NSError errorWithDomain:@"Configuration"
                                                             code:0
                                                         userInfo:@{@"User Info":@"No Data"}]);
        return;
    }
    
    for (NSDictionary *dict in results) {
        TMDBMovie *movie = [[TMDBMovie alloc] initWithDictionary:dict andConfiguration:config];
        [result_converted addObject:movie];
    }
    
    NSString *page = [d objectForKey:@"page"];
    [pageItem setObject:result_converted forKey:page];
//    TMDBConfiguration *configuration = [[TMDBConfiguration alloc] initWithDictionary:d];

    safe_block(self.completion)(pageItem, nil);
}



@end
