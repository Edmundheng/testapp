//
//  AppDelegate.m
//  testApp
//
//  Created by Edmund Heng on 30/11/20.
//

#import "AppDelegate.h"
#import "DBHelper.h"
#import "TMDBHelper.h"
#import "TMDBMovie.h"

@interface AppDelegate ()
@property (nonatomic, assign) BOOL searchMode;
@property (nonatomic, strong) NSMutableDictionary *preDefinedMovieList;
@property (nonatomic, strong) NSString *currentSearchText;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [DBHelper createDatabase];
    [self initializeData];
    self.searchMode = NO;
    return YES;
}

#pragma mark - Data Methods

-(void)setSearchMode:(BOOL)searchMode {
    _searchMode = searchMode;
}

-(void)startSearch:(NSString *)searchText {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (!self.searchMode) {
            self.searchMode = YES;
            self.preDefinedMovieList = [NSMutableDictionary dictionaryWithDictionary:self.movieList];
        }
        self.currentSearchText = searchText;
        NSMutableDictionary *finalResults = [NSMutableDictionary new];
        if (searchText.length > 0) {
            for (NSString *key in [self.preDefinedMovieList allKeys]) {
                NSArray *arrayToSearch = [self.preDefinedMovieList objectForKey:key];
                NSArray *filtered = [self filteredArrayForSearchMode:arrayToSearch];
                [finalResults setObject:filtered forKey:key];
            }
            
            if (finalResults.count) {
                [DBHelper logSearch:searchText];
            }
        } else {
            finalResults = [self.preDefinedMovieList mutableCopy];
        }
        
        self.movieList = finalResults;
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:movieListUpdatedNotification object:self.movieList];
        });
    });
    
}

-(NSArray *) filteredArrayForSearchMode:(NSArray *)arrayToSearch {
    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        TMDBMovie *movie = (TMDBMovie *)evaluatedObject;
        
        if ([[movie.originalTitle lowercaseString] containsString:[self.currentSearchText lowercaseString]] || [[movie.title lowercaseString] containsString:[self.currentSearchText lowercaseString]]) {
            return YES;
        }
        return NO;
    }];
    NSArray *filtered = [arrayToSearch filteredArrayUsingPredicate:predicate] ?: [NSArray new];
    return filtered;
}

-(void)clearSearchMode {
    if (self.searchMode) {
        self.searchMode = NO;
        self.movieList = [NSMutableDictionary dictionaryWithDictionary:self.preDefinedMovieList];
        self.preDefinedMovieList = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:movieListUpdatedNotification object:self.movieList];
    }
}

#pragma mark - Data Populating

- (void)updateMovieList:(NSDictionary *)movieList currentPage:(int)page andGenre:(int)genre {
    if (genre != -1) {
        [DBHelper logGenreTouch:[NSString stringWithFormat:@"%i",genre]];
    }
    [self populateMovieList:movieList];
    int nextPage = page+1;
    self.currentGenre = genre;
    
    [self getMoreMovies:nextPage];
}

- (void)populateMovieList:(NSDictionary *)movieList {
    for (NSString *key in [movieList allKeys]) {
        NSArray *filteredArray = [movieList objectForKey:key];
        if (self.searchMode) {
            [self.preDefinedMovieList setObject:[movieList objectForKey:key] forKey:key];
            filteredArray = [self filteredArrayForSearchMode:filteredArray];
        }
        [self.movieList setObject:filteredArray forKey:key];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:movieListUpdatedNotification object:self.movieList];
}

- (void) getMoreMovies:(int)pageToRetrieve {
    [TMDBHelper searchForGenre:self.currentGenre page:pageToRetrieve handler:^(id item, NSError * _Nullable error) {
        if (item)
            [self populateMovieList:(NSDictionary *)item];
    }];
}

#pragma mark - Data initialization

-(void) initializeData {
    [self setupConfiguration];
    [self setupGenreList];
    self.movieList = [NSMutableDictionary new];
}

-(void) setupConfiguration {
    TMDBConfiguration *configuration = [self unarchiveObjectWithFilename:configurationKey];
    self.configuration = configuration;
    
    [TMDBHelper getConfiguration:^(id item, NSError *error) {
        if (item) {
            TMDBConfiguration *configuration = (TMDBConfiguration *)item;
            self.configuration = configuration;
            [self archiveRootObject:configuration toFileWithName:configurationKey];
        } else if (!self.configuration){
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Unexpected error occurred loading configuration, please relaunch the app." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                exit(0);
            }];

            [alert addAction:defaultAction];
            [[[self window] rootViewController] presentViewController:alert animated:YES completion:nil];
        }
    }];
}

-(void) setupGenreList {
    [DBHelper getListOfGenres:^(id item, NSError * _Nullable error) {
        self.genreList = (NSArray *)item;
        [[NSNotificationCenter defaultCenter] postNotificationName:genresUpdatedNotification object:self.genreList];
    }];
    
    [TMDBHelper getGenreList:^(id item, NSError * _Nullable error) {
        self.genreList = (NSArray *)item;
        [DBHelper insertOrUpdateGenreList:self.genreList];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:genresUpdatedNotification object:self.genreList];
        });
    }];
}


#pragma mark - UISceneSession lifecycle


- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
}


- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions {
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
}

#pragma mark - Root Object writing

- (BOOL)archiveRootObject:(id)rootObject toFileWithName:(NSString *)filename
{
    NSString *docsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [docsPath stringByAppendingPathComponent:filename];
    return [NSKeyedArchiver archiveRootObject:rootObject toFile:filePath];
}

- (id)unarchiveObjectWithFilename:(NSString *)filename
{
    NSString *docsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [docsPath stringByAppendingPathComponent:filename];
    return [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
}


@end
