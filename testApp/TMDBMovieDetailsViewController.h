//
//  TMDBMovieDetailsViewController.h
//  testApp
//
//  Created by Edmund Heng on 3/12/20.
//

#import <UIKit/UIKit.h>
#import "TMDBMovie.h"
#import <youtube_ios_player_helper/youtube-ios-player-helper-umbrella.h>

NS_ASSUME_NONNULL_BEGIN

@class TMDBMovieDetailsViewController;

@protocol TMDBMovieDetailsDelegate <NSObject>

- (void)loadDetailsForMovie:(TMDBMovieDetailsViewController *)controller;

@end

@interface TMDBMovieDetailsViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,YTPlayerViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *backdropImageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *posterImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *DetailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *yearLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *tagLineLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightForStackView;

@property (strong, nonatomic) TMDBMovie *movie;

@property (strong, nonatomic) id<TMDBMovieDetailsDelegate>detailDelegate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeightConstraint;

-(void) populateData:(TMDBMovie *)movie;
-(void) updateViews;

@end

NS_ASSUME_NONNULL_END
