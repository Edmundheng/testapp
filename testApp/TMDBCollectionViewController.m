//
//  TMDBCollectionViewController.m
//  testApp
//
//  Created by Edmund Heng on 2/12/20.
//

#import "TMDBCollectionViewController.h"
#import "AppDelegate.h"
#import "UILoadingView.h"
#import "TMDBMovie.h"
#import "movieCollectionCell.h"

@interface TMDBCollectionViewController ()
@property (nonatomic,weak) NSDictionary *movieList;
@property (nonatomic,strong) UILoadingView *loadingView;
@property (strong, nonatomic) IBOutlet UIView *searchResultsErrorView;

@end

@implementation TMDBCollectionViewController

static NSString * const reuseIdentifier = @"movieCollectionCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
//    [self.collectionView registerClass:[movieCollectionCell class] forCellWithReuseIdentifier:reuseIdentifier];
    UIImage *background = [UIImage imageNamed:@"starsTemplate"];
    self.collectionView.backgroundColor = [UIColor colorWithPatternImage:background];
    self.collectionView.contentInset =UIEdgeInsetsMake(15.0f, 15.0f, 15.0f, 15.0f);
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (!self.movieList) {
        [self setupLoadingView];
    }
}


-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

#pragma mark - Controller methods

-(void)setAppeared:(BOOL)appeared {
    _appeared = appeared;
    if (appeared) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(movieListUpdated:)
                                                     name:movieListUpdatedNotification
                                                   object:nil];
    } else {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    
}

- (void) requestForDataUpdate {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [appDelegate getMoreMovies:(int)self.movieList.allKeys.count+1];
}

- (void) updateDataAndView {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.movieList = appDelegate.movieList;
}

#pragma mark - Controller setup

- (void) setupLoadingView {
    if (!self.loadingView) {
        UILoadingView *loading = [UILoadingView new];
        loading.frame = self.collectionView.frame;
        [self.view addSubview:loading];
        [loading layoutSubviews];
        self.loadingView = loading;
    }
}

- (void) removeLoadingView {
    if (self.loadingView) {
        [self.loadingView removeFromSuperview];
        self.loadingView = nil;
    }
}

#pragma mark - Data setup

-(void) setMovieList:(NSDictionary *)movieList {
    _movieList = movieList;
    [self removeLoadingView];
    NSMutableArray *moviesSorted = [NSMutableArray new];
    for (int i=1;i<=movieList.allKeys.count;i++) {
        NSArray *array = [movieList objectForKey:@(i)];
        [moviesSorted addObjectsFromArray:array];
    }
    if (moviesSorted.count > 0) {
        if (self.searchResultsErrorView.superview)
            [self.searchResultsErrorView removeFromSuperview];
    } else {
        if (!self.searchResultsErrorView.superview) {
            self.searchResultsErrorView.frame = self.view.frame;
            [self.view addSubview:self.searchResultsErrorView];
        }
    }
    [self.collectionView reloadData];
}

#pragma mark - Notification observers
- (void)movieListUpdated:(NSNotification*)notification {
    self.movieList = (NSMutableDictionary *)notification.object;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [self.movieList allKeys].count;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSArray *array = [self.movieList objectForKey:@(section+1)];
    return array.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    movieCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    NSArray *array = [self.movieList objectForKey:@(indexPath.section+1)];
    TMDBMovie *movie = [array objectAtIndex:indexPath.row];
    // Configure the cell
    [cell updateWithMovie:movie];
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize screenSize =  [[UIScreen mainScreen] bounds].size;
    double itemWidth = (screenSize.width-15.0f-15.0f-10.0f) / 2;
    double itemHeight = itemWidth/55*100;
    CGSize itemSize = CGSizeMake(itemWidth, itemHeight);
    return itemSize;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 10.0f;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 0, 0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    int numberOfSections = (int)self.movieList.allKeys.count;
    if (indexPath.section == numberOfSections-1) {
        //last section
        NSArray *array = [self.movieList objectForKey:@(numberOfSections)];
        if (indexPath.row == array.count - 1 ) { //it's your last cell
           //Load more data & reload your collection view
            [self requestForDataUpdate];
        }
    }
    
}

#pragma mark <UICollectionViewDelegate>


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate) {
        [self.delegate tapForDetails:indexPath];
    }
}
/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
 
*/



@end
