//
//  TMDBViewPagerController.h
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import <CKViewPager/ViewPagerController.h>
#import "TMDBMovieDetailsViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TMDBViewPagerController : ViewPagerController <ViewPagerDelegate, ViewPagerDataSource, TMDBMovieDetailsDelegate>

@property (nonatomic, assign) int currentIndex;
@property (nonatomic, assign) BOOL appeared;
@property (strong, nonatomic) IBOutlet UIView *searchResultErrorView;

-(void) updateDataAndView:(NSIndexPath *)chosenPath;
@end

NS_ASSUME_NONNULL_END
