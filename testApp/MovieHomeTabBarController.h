//
//  MovieHomeTabBarController.h
//  testApp
//
//  Created by Edmund Heng on 30/11/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MovieHomeTabBarController : UITabBarController

@end

NS_ASSUME_NONNULL_END
