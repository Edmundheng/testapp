//
//  searchTrailViewController.m
//  testApp
//
//  Created by Edmund Heng on 4/12/20.
//

#import "searchTrailViewController.h"
#import "DBHelper.h"
#import "AppDelegate.h"
#import "MovieHomeViewController.h"
@interface searchTrailViewController ()
@property (nonatomic, strong) NSArray *searchResults;
@property (nonatomic, weak) NSArray *genreList;
@end

@implementation searchTrailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"searchTrail"];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.genreList = appDelegate.genreList;
    
    [DBHelper getSearchTrail:^(id item, NSError * _Nullable error) {
        if (item) {
            self.searchResults = (NSArray *)item;
        }
    }];
}

-(void)setSearchResults:(NSArray *)searchResults {
    _searchResults = searchResults;
    [self.tableView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"searchTrail"];
    NSDictionary *d = [self.searchResults objectAtIndex:indexPath.row];
    int searchType = [[d objectForKey:@"searchType"] intValue];
    NSString *searchString = @"";
    
    switch (searchType) {
        case 1:
            searchString = [NSString stringWithFormat:@"the text (%@)", [d objectForKey:@"searchText"]];
            break;
        case 2:{
            int value = [[d objectForKey:@"searchText"] intValue];
            NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
                NSDictionary *dict = (NSDictionary *)evaluatedObject;
                int identifier = [[dict objectForKey:@"id"] intValue];
                return value == identifier;
            }];
            NSArray *filtered = [self.genreList filteredArrayUsingPredicate:predicate];
            
            if (filtered.count) {
                NSDictionary *dict = [filtered firstObject];
                NSString *genreName = [dict objectForKey:@"name"];
                searchString = [NSString stringWithFormat:@"the genre (%@)",genreName];
            }
        }
            break;
        default:
            break;
    }
    cell.textLabel.text = [NSString stringWithFormat:@"I have searched for %@",searchString];
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.searchResults.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 30;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.tabBarController.selectedIndex = 0;
    MovieHomeViewController *vc = [self.tabBarController.viewControllers firstObject];
    
    NSDictionary *d = [self.searchResults objectAtIndex:indexPath.row];
    int searchType = [[d objectForKey:@"searchType"] intValue];
    NSString *searchString = [d objectForKey:@"searchText"];
    
    switch (searchType) {
        case 1:
            [vc startSearch:searchString];
            break;
        case 2:{
            int value = [searchString intValue];
            [vc startSearchForGenre:value];
        }
            break;
        default:
            break;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
