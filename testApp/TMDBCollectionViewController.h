//
//  TMDBCollectionViewController.h
//  testApp
//
//  Created by Edmund Heng on 2/12/20.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol movieTouchDelegate <NSObject>

- (void)tapForDetails:(NSIndexPath *)path;

@end

@interface TMDBCollectionViewController : UICollectionViewController <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) id<movieTouchDelegate>delegate;
@property (nonatomic, assign) BOOL appeared;

-(void) updateDataAndView;

@end

NS_ASSUME_NONNULL_END
