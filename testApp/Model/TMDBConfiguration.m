//
//  TMDBConfiguration.m
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import "TMDBConfiguration.h"

@implementation TMDBConfiguration


# pragma mark - Instantion helpers
- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:dictionary];
        TMDBConfigurationImages *imagesConfig = [[TMDBConfigurationImages alloc] initWithDictionary:[dictionary objectForKey:@"images"]];
        self.images = imagesConfig;
    }
    
    return self;
}

#pragma mark - NSCoder

- (instancetype)initWithCoder:(NSCoder *)decoder {

    self = [self init];
    if (!self) {
        return nil;
    }
    
    self.change_keys = [decoder decodeObjectForKey:@"change_keys"];
    self.images = [decoder decodeObjectForKey:@"images"];

    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    if (self.change_keys)
        [encoder encodeObject:self.change_keys forKey:@"change_keys"];
    if (self.images)
        [encoder encodeObject:self.images forKey:@"images"];
}


@end
