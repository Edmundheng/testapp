//
//  TMDBMoviePreview.h
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import <Foundation/Foundation.h>
#import "TMDBConfiguration.h"

typedef NS_ENUM(NSInteger, siteType) {
    siteTypeYoutube,
    siteTypeVimeo,
    siteTypeUnknown = 0
};

NS_ASSUME_NONNULL_BEGIN

@interface TMDBMoviePreview : NSObject

@property (assign, nonatomic) siteType site;
@property (strong, nonatomic) NSString *siteID;
@property (assign, nonatomic) int size;
@property (strong, nonatomic) NSString *iso_639_1;

@property (strong, nonatomic) NSString *key;
@property (strong, nonatomic) NSString *iso_3166_1;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *video_type;



//@property (strong, nonatomic) NSString *backdrop_path;


- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END
