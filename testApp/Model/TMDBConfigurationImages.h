//
//  TMDBConfigurationImages.h
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, imageBackdropSizes) {
    imageBackdropSizesOriginal = 0,
    imageBackdropSizes300,
    imageBackdropSizes780,
    imageBackdropSizes1280
};

typedef NS_ENUM(NSInteger, imageLogoSizes) {
    imageLogoSizesOriginal = 0,
    imageLogoSizesw45,
    imageLogoSizesw92,
    imageLogoSizesw154,
    imageLogoSizesw185,
    imageLogoSizesw300,
    imageLogoSizesw500
};

typedef NS_ENUM(NSInteger, imagePosterSizes) {
    imagePosterSizesOriginal = 0,
    imagePosterSizes92,
    imagePosterSizes154,
    imagePosterSizes185,
    imagePosterSizes342,
    imagePosterSizes500,
    imagePosterSizes780
};

typedef NS_ENUM(NSInteger, imageProfileSizes) {
    imageProfileSizesOriginal = 0,
    imageProfileSizes45,
    imageProfileSizes185,
    imageProfileSizes632
};

typedef NS_ENUM(NSInteger, imageStillSizes) {
    imageStillSizesOriginal = 0,
    imageStillSizes92,
    imageStillSizes185,
    imageStillSizes300
};

NS_ASSUME_NONNULL_BEGIN

@interface TMDBConfigurationImages : NSObject

@property (strong, nonatomic) NSArray *backdrop_sizes;
@property (strong, nonatomic) NSString *base_url;
@property (strong, nonatomic) NSArray *logo_sizes;
@property (strong, nonatomic) NSArray *poster_sizes;
@property (strong, nonatomic) NSArray *profile_sizes;
@property (strong, nonatomic) NSArray *still_sizes;
@property (strong, nonatomic) NSString *secure_base_url;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END
