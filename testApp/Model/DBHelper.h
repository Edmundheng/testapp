//
//  DBHelper.h
//  testApp
//
//  Created by Edmund Heng on 30/11/20.
//

#import <Foundation/Foundation.h>

#define safe_block(_block) if(_block) _block
typedef void (^DBHandler) (id item, NSError * _Nullable error);

NS_ASSUME_NONNULL_BEGIN

@interface DBHelper : NSObject

+ (void) createDatabase;
+ (void) insertOrUpdateGenreList:(NSArray *)genreList;
+ (void) getListOfGenres:(DBHandler)handler;

+ (void) getSearchTrail:(DBHandler)handler;
+ (void) logSearch:(NSString *)searchText;
+ (void) logGenreTouch:(NSString *)genre;

@end

NS_ASSUME_NONNULL_END
