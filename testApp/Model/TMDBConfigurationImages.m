//
//  TMDBConfigurationImages.m
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import "TMDBConfigurationImages.h"

@implementation TMDBConfigurationImages

# pragma mark - Instantion helpers
- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self)
        [self setValuesForKeysWithDictionary:dictionary];
    
    
    return self;
}

#pragma mark - NSCoding

- (instancetype)initWithCoder:(NSCoder *)decoder {

    self = [self init];
    if (!self) {
        return nil;
    }
    
    self.backdrop_sizes = [decoder decodeObjectForKey:@"backdrop_sizes"];
    self.base_url = [decoder decodeObjectForKey:@"base_url"];
    self.logo_sizes = [decoder decodeObjectForKey:@"logo_sizes"];
    self.poster_sizes = [decoder decodeObjectForKey:@"poster_sizes"];
    self.profile_sizes = [decoder decodeObjectForKey:@"profile_sizes"];
    self.secure_base_url = [decoder decodeObjectForKey:@"secure_base_url"];

    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    if (self.backdrop_sizes)
        [encoder encodeObject:self.backdrop_sizes forKey:@"backdrop_sizes"];
    if (self.base_url)
        [encoder encodeObject:self.base_url forKey:@"base_url"];
    if (self.logo_sizes)
        [encoder encodeObject:self.logo_sizes forKey:@"logo_sizes"];
    if (self.poster_sizes)
        [encoder encodeObject:self.poster_sizes forKey:@"poster_sizes"];
    if (self.profile_sizes)
        [encoder encodeObject:self.profile_sizes forKey:@"profile_sizes"];
    if (self.secure_base_url)
        [encoder encodeObject:self.secure_base_url forKey:@"secure_base_url"];
}

@end
