//
//  TMDBMovie.m
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import "TMDBMovie.h"

@implementation TMDBMovie


# pragma mark - Instantion helpers
- (instancetype)initWithDictionary:(NSDictionary *)dictionary andConfiguration:(nonnull TMDBConfiguration *)configuration
{
    self = [super init];
    if (self) {
        NSString *baseURL = configuration.images.base_url;
        NSString *imageSizeForBackdrop = @"w1280";
        NSString *imageSizeForPoster = @"w500";
        NSString *backdropString = [NSString stringWithFormat:@"%@%@%@",baseURL,imageSizeForBackdrop,[dictionary objectForKey:@"backdrop_path"]];
        NSString *posterString = [NSString stringWithFormat:@"%@%@%@",baseURL,imageSizeForPoster,[dictionary objectForKey:@"poster_path"]];
        
        
        self.backdropImageURL = [NSURL URLWithString:backdropString];
        self.posterImageURL = [NSURL URLWithString:posterString];
        
        self.movieID = [[dictionary objectForKey:@"id"] intValue];
        self.genreIDs = [dictionary objectForKey:@"genre_ids"];
        self.originalTitle = [dictionary objectForKey:@"original_title"];
        self.overview = [dictionary objectForKey:@"overview"];
        self.title = [dictionary objectForKey:@"title"];
        self.original_language = [dictionary objectForKey:@"original_language"];
        self.releaseDate = [dictionary objectForKey:@"release_date"];
        self.voteAverage = [[dictionary objectForKey:@"vote_average"] intValue];
        self.voteCount = [[dictionary objectForKey:@"vote_count"] intValue];
        self.isAdult = [[dictionary objectForKey:@"adult"] boolValue];
        self.hasVideo = [[dictionary objectForKey:@"video"] boolValue];
        self.popularity = [[dictionary objectForKey:@"popularity"] doubleValue];
        
    }
    
    return self;
}

- (void) populateDetails:(NSDictionary *)dictionary {
    if ([dictionary objectForKey:@"runtime"] != [NSNull null]){
        self.runTime = [[dictionary objectForKey:@"runtime"] intValue];
    } else {
        self.runTime = 0;
    }
    self.status = [dictionary objectForKey:@"status"];
    self.tagLine = [dictionary objectForKey:@"tagline"];
}

#pragma mark - NSCoder

- (instancetype)initWithCoder:(NSCoder *)decoder {

    self = [self init];
    if (!self) {
        return nil;
    }
    
    self.movieID = [decoder decodeIntForKey:@"movieID"];
    self.backdropImageURL = [decoder decodeObjectForKey:@"backdropImageURL"];
    self.posterImageURL = [decoder decodeObjectForKey:@"posterImageURL"];
    self.genreIDs = [decoder decodeObjectForKey:@"genreIDs"];
    self.originalTitle = [decoder decodeObjectForKey:@"originalTitle"];
    self.overview = [decoder decodeObjectForKey:@"overview"];
    self.title = [decoder decodeObjectForKey:@"title"];
    self.original_language = [decoder decodeObjectForKey:@"original_language"];
    self.releaseDate = [decoder decodeObjectForKey:@"releaseDate"];
    self.voteAverage = [decoder decodeIntForKey:@"voteAverage"];
    self.voteCount = [decoder decodeIntForKey:@"voteCount"];
    self.isAdult = [decoder decodeBoolForKey:@"isAdult"];
    self.hasVideo = [decoder decodeIntForKey:@"hasVideo"];
    self.popularity = [decoder decodeDoubleForKey:@"popularity"];
    
    //details
    self.runTime = [decoder decodeIntForKey:@"runTime"];
    self.status = [decoder decodeObjectForKey:@"status"];
    self.tagLine = [decoder decodeObjectForKey:@"tagLine"];
    //preview
    self.previews = [decoder decodeObjectForKey:@"previews"];

    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInt:self.movieID forKey:@"movieID"];
    if (self.backdropImageURL)
        [encoder encodeObject:self.backdropImageURL forKey:@"backdropImageURL"];
    if (self.posterImageURL)
        [encoder encodeObject:self.posterImageURL forKey:@"posterImageURL"];
    if (self.genreIDs)
        [encoder encodeObject:self.genreIDs forKey:@"genreIDs"];
    if (self.originalTitle)
        [encoder encodeObject:self.originalTitle forKey:@"originalTitle"];
    if (self.overview)
        [encoder encodeObject:self.overview forKey:@"overview"];
    if (self.title)
        [encoder encodeObject:self.title forKey:@"title"];
    if (self.original_language)
        [encoder encodeObject:self.original_language forKey:@"original_language"];
    if (self.releaseDate)
        [encoder encodeObject:self.releaseDate forKey:@"releaseDate"];
    
    [encoder encodeInt:self.voteAverage forKey:@"voteAverage"];
    [encoder encodeInt:self.voteCount forKey:@"voteCount"];
    
    [encoder encodeBool:self.isAdult forKey:@"isAdult"];
    [encoder encodeBool:self.hasVideo forKey:@"hasVideo"];
    
    [encoder encodeDouble:self.popularity forKey:@"popularity"];
    
    [encoder encodeInt:self.runTime forKey:@"runTime"];
    if (self.status)
        [encoder encodeObject:self.status forKey:@"status"];
    if (self.tagLine)
        [encoder encodeObject:self.tagLine forKey:@"tagLine"];
    
    if (self.previews)
        [encoder encodeObject:self.previews forKey:@"previews"];
}


@end
