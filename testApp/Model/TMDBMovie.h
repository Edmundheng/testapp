//
//  TMDBMovie.h
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import <Foundation/Foundation.h>
#import "TMDBConfiguration.h"
#import "TMDBMoviePreview.h"

NS_ASSUME_NONNULL_BEGIN

@interface TMDBMovie : NSObject

@property (assign, nonatomic) int movieID;
@property (strong, nonatomic) NSURL *backdropImageURL;
@property (strong, nonatomic) NSURL *posterImageURL;
@property (strong, nonatomic) NSArray *genreIDs;
@property (strong, nonatomic) NSString *originalTitle;
@property (strong, nonatomic) NSString *overview;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *original_language;
@property (strong, nonatomic) NSString *releaseDate;
@property (assign, nonatomic) int voteAverage;
@property (assign, nonatomic) int voteCount;
@property (assign, nonatomic) BOOL isAdult;
@property (assign, nonatomic) BOOL hasVideo;
@property (assign, nonatomic) double popularity;

//details
@property (assign, nonatomic) int runTime;
@property (strong, nonatomic) NSString *status;
@property (strong, nonatomic) NSString *tagLine;

//preview
@property (strong, nonatomic) NSArray *previews;

//@property (strong, nonatomic) NSString *backdrop_path;


- (instancetype)initWithDictionary:(NSDictionary *)dictionary andConfiguration:(TMDBConfiguration *)configuration;
- (void) populateDetails:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END
