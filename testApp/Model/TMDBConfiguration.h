//
//  TMDBConfiguration.h
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import <Foundation/Foundation.h>
#import "TMDBConfigurationImages.h"

NS_ASSUME_NONNULL_BEGIN

@interface TMDBConfiguration : NSObject

@property (strong, nonatomic) NSArray *change_keys;
@property (strong, nonatomic) TMDBConfigurationImages *images;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END
