//
//  TMDBMoviePreview.m
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import "TMDBMoviePreview.h"

@implementation TMDBMoviePreview


# pragma mark - Instantion helpers
- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if (self) {
        NSString *theSite = [dictionary objectForKey:@"site"];
        if ([theSite isEqualToString:@"YouTube"]) {
            self.site = siteTypeYoutube;
        } else if ([theSite isEqualToString:@"Vimeo"]){
            self.site = siteTypeVimeo;
        } else {
        }
        
        self.siteID = [dictionary objectForKey:@"id"];
        self.size = [[dictionary objectForKey:@"size"] intValue];
        self.iso_639_1 =[dictionary objectForKey:@"iso_639_1"];
        self.key = [dictionary objectForKey:@"key"];
        self.iso_3166_1 = [dictionary objectForKey:@"iso_3166_1"];
        self.name = [dictionary objectForKey:@"name"];
        self.video_type = [dictionary objectForKey:@"type"];
    }
    
    return self;
}

#pragma mark - NSCoder

- (instancetype)initWithCoder:(NSCoder *)decoder {

    self = [self init];
    if (!self) {
        return nil;
    }
    
    self.site = [decoder decodeIntForKey:@"site"];
    self.size = [decoder decodeIntForKey:@"size"];
    
    self.siteID = [decoder decodeObjectForKey:@"siteID"];
    self.iso_639_1 = [decoder decodeObjectForKey:@"iso_639_1"];
    self.key = [decoder decodeObjectForKey:@"key"];
    self.iso_3166_1 = [decoder decodeObjectForKey:@"iso_3166_1"];
    self.name = [decoder decodeObjectForKey:@"name"];
    self.video_type = [decoder decodeObjectForKey:@"video_type"];

    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    if (self.siteID)
        [encoder encodeObject:self.siteID forKey:@"siteID"];
    if (self.iso_639_1)
        [encoder encodeObject:self.iso_639_1 forKey:@"iso_639_1"];
    if (self.key)
        [encoder encodeObject:self.key forKey:@"key"];
    if (self.iso_3166_1)
        [encoder encodeObject:self.iso_3166_1 forKey:@"iso_3166_1"];
    if (self.name)
        [encoder encodeObject:self.name forKey:@"name"];
    if (self.video_type)
        [encoder encodeObject:self.video_type forKey:@"video_type"];
}


@end
