//
//  DBHelper.m
//  testApp
//
//  Created by Edmund Heng on 30/11/20.
//

#import "DBHelper.h"
#import "DBConnection.h"

@implementation DBHelper

+(void) createDatabase {
    if([DBConnection executeQuery:@"CREATE TABLE IF NOT EXISTS genreList (id INTEGER PRIMARY KEY NOT NULL, name TEXT NOT NULL)"]){
//        NSLog(@"created genre list");
    }else{
        NSLog(@"CREATE TABLE IF NOT EXISTS product (id INTEGER PRIMARY KEY NOT NULL, name TEXT NOT NULL)");
    }
    
    if([DBConnection executeQuery:@"CREATE TABLE IF NOT EXISTS searchResults (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, searchText TEXT NOT NULL, searchType INTEGER NOT NULL)"]){
//        NSLog(@"created searchResults");
    }else{
        NSLog(@"CREATE TABLE IF NOT EXISTS searchResults (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, searchText TEXT NOT NULL, searchType INTEGER NOT NULL)");
    }
    
}

+ (void) insertOrUpdateGenreList:(NSArray *) genreList {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (NSDictionary * genre in genreList) {
            NSNumber *number = [genre objectForKey:@"id"];
            NSString *name = [genre objectForKey:@"name"];
            NSString *statement = [NSString stringWithFormat:@"INSERT OR REPLACE INTO genreList(id,name) VALUES(%@,\"%@\")",number,name];
            if ([DBConnection executeQuery:statement]) {
//                NSLog(@"inserted successfully");
            } else {
                NSLog(@"error");
            }
        }
    });
}

+ (void) getListOfGenres:(DBHandler)handler {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *statement = @"SELECT * FROM genreList";
        NSArray *genres = [DBConnection fetchResults:statement];
        dispatch_sync(dispatch_get_main_queue(), ^{
            safe_block(handler)(genres, genres ? nil : [NSError errorWithDomain:@"SQL Error" code:0 userInfo:@{@"User Info":@"No Data"}]);
        });
    });
}

+ (void) logSearch:(NSString *)searchText {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *statement = [NSString stringWithFormat:@"INSERT OR REPLACE INTO searchResults(searchText,searchType) VALUES(\"%@\",%@)",searchText,@1];
        if ([DBConnection executeQuery:statement]) {
//                NSLog(@"inserted successfully");
        } else {
            NSLog(@"error");
        }
    });
}

+ (void) logGenreTouch:(NSString *)genre {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *statement = [NSString stringWithFormat:@"INSERT OR REPLACE INTO searchResults(searchText,searchType) VALUES(\"%@\",%@)",genre,@2];
        if ([DBConnection executeQuery:statement]) {
//                NSLog(@"inserted successfully");
        } else {
            NSLog(@"error");
        }
    });
}

+ (void) getSearchTrail:(DBHandler)handler {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *statement = @"SELECT * FROM searchResults";
        NSArray *results = [DBConnection fetchResults:statement];
        dispatch_sync(dispatch_get_main_queue(), ^{
            safe_block(handler)(results, results ? nil : [NSError errorWithDomain:@"SQL Error" code:0 userInfo:@{@"User Info":@"No Data"}]);
        });
    });
}

@end
