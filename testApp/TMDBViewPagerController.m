//
//  TMDBViewPagerController.m
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import "TMDBViewPagerController.h"
#import "AppDelegate.h"
#import "TMDBMovie.h"
#import "TMDBHelper.h"


@interface TMDBViewPagerController ()

@property (nonatomic,weak) NSMutableDictionary *movieList;
@property (nonatomic, strong) NSMutableArray *moviesSorted;
@property (nonatomic, strong) NSIndexPath *chosenPath;

@property (nonatomic, assign) BOOL requestDataUpdate;
@end

@implementation TMDBViewPagerController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataSource = self;
    self.delegate = self;
    self.tabLocation = ViewPagerTabLocationBottom;
    self.currentIndex = 0;
    self.fixLatterTabsPositions = YES;
    UIImage *background = [UIImage imageNamed:@"starsTemplate"];
    self.view.backgroundColor = [UIColor colorWithPatternImage:background];
//    self.tabOffset = 300.0f;
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void)setAppeared:(BOOL)appeared {
    _appeared = appeared;
    if (appeared) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(movieListUpdated:)
                                                     name:movieListUpdatedNotification
                                                   object:nil];
    } else {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Controller Methods

- (void)movieListUpdated:(NSNotification*)notification {
    self.movieList = (NSMutableDictionary *)notification.object;
    self.requestDataUpdate = NO;
}

- (void) requestForDataUpdate {
    self.requestDataUpdate = YES;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [appDelegate getMoreMovies:(int)self.movieList.allKeys.count+1];
}

-(void) updateDataAndView:(NSIndexPath *)chosenPath
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    self.movieList = appDelegate.movieList;
    self.chosenPath = chosenPath;
    NSInteger section = chosenPath.section;
    NSInteger row = chosenPath.row;
    NSArray *array = [self.movieList objectForKey:@(section+1)];
    TMDBMovie *movie;
    if (array.count > row)
        movie = [array objectAtIndex:row];
    NSInteger index = [self.moviesSorted indexOfObject:movie];
    [self selectTabAtIndex:index];
}

- (void) setMovieList:(NSMutableDictionary *)movieList {
    _movieList = movieList;
    self.moviesSorted = [NSMutableArray new];
    for (int i=1;i<=movieList.allKeys.count;i++) {
        NSArray *array = [movieList objectForKey:@(i)];
        [self.moviesSorted addObjectsFromArray:array];
    }
    
    if (self.currentIndex == -1) {
        [self resetData];
        [self selectTabAtIndex:0 animated:NO];
    } else {
        [self reloadData];
        [self selectTabAtIndex:self.currentIndex animated:NO];
    }
    
    if (self.moviesSorted.count > 0) {
        if (self.searchResultErrorView.superview)
            [self.searchResultErrorView removeFromSuperview];
    } else {
        if (!self.searchResultErrorView.superview) {
            self.searchResultErrorView.frame = self.view.frame;
            [self.view addSubview:self.searchResultErrorView];
        }
    }
}

#pragma mark - ViewPagerDataSource
- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
    return self.moviesSorted.count;
}

- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {

    UILabel *label = [UILabel new];
    [label setFont:[UIFont systemFontOfSize:10.0f]];
    TMDBMovie *movie = [self.moviesSorted objectAtIndex:index];
    label.text = movie.title;
    [label sizeToFit];

    return label;
}

//-(UIView *)viewPager:(ViewPagerController *)viewPager contentViewForTabAtIndex:(NSUInteger)index {
////    TMDBMovieDetailsView *details = [self.movieDetailsViewTemplate copy];
//
////    view.backgroundColor = UIColor.greenColor;
//    return details;
//}

- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    TMDBMovieDetailsViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"TMDBMovieDetailsViewController"];
    TMDBMovie *movie = [self.moviesSorted objectAtIndex:index];
    viewController.detailDelegate = self;
    [viewController populateData:movie];
//    viewController.view.backgroundColor = [UIColor blueColor];
    return viewController;
}

- (void)viewPager:(ViewPagerController *)viewPager didChangeTabToIndex:(NSUInteger)index {
    self.currentIndex = index;
    if (self.moviesSorted.count-1-index < 20 && !self.requestDataUpdate) {
        //load in new items;
        [self requestForDataUpdate];
    }
    // Do something useful
}


#pragma mark - Detail delegate

-(void)loadDetailsForMovie:(TMDBMovieDetailsViewController *)controller {
    if (!controller.movie.tagLine) {
        [TMDBHelper getDetailsForMovie:controller.movie handler:^(id item, NSError * _Nullable error) {
            [controller updateViews];
        }];
    }
    
    if (!controller.movie.previews) {
        [TMDBHelper getPreviewsForMovie:controller.movie handler:^(id item, NSError * _Nullable error) {
            [controller updateViews];
        }];
    }
}



@end
