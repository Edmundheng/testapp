//
//  TMDBMovieDetailsViewController.m
//  testApp
//
//  Created by Edmund Heng on 3/12/20.
//

#import "TMDBMovieDetailsViewController.h"
#import <SDWebImage/SDWebImage.h>
#import "TMDBMovie.h"


@interface TMDBMovieDetailsViewController ()
@property (assign, nonatomic) BOOL updatedVideos;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation TMDBMovieDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.backdropImageView.alpha = 0.4;
    self.collectionView.contentInset =UIEdgeInsetsMake(0, 15.0f, 0, 15.0f);
    [self populateView];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.detailDelegate) {
        [self.detailDelegate loadDetailsForMovie:self];
    }
}

- (void) updateViews {
    self.statusLabel.text = self.movie.status;
    self.durationLabel.text = self.movie.runTime > 0 ? [NSString stringWithFormat:@"%i",self.movie.runTime] : @"";
    self.tagLineLabel.text = self.movie.tagLine;
    
    [self.collectionView reloadData];
    [self.view layoutIfNeeded];
    
}

-(void) populateData:(TMDBMovie *)movie {
//    @property (weak, nonatomic) IBOutlet UIImageView *backdropImageView;
//    @property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
//    @property (weak, nonatomic) IBOutlet UIView *posterImageView;
//    @property (weak, nonatomic) IBOutlet UILabel *titleLabel;
//    @property (weak, nonatomic) IBOutlet UILabel *DetailsLabel;
    self.movie = movie;
}

-(void) populateView {
    if(self.movie) {
        UIImage *backdropPlaceHolder = [UIImage imageNamed:@"starsTemplate"];
        UIImage *placeholder = [UIImage imageNamed:@"icon_placeholder"];
        [self.backdropImageView sd_setImageWithURL:self.movie.backdropImageURL placeholderImage:backdropPlaceHolder completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            
        }];
        
        [self.posterImageView sd_setImageWithURL:self.movie.posterImageURL placeholderImage:placeholder completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        }];
        
        self.titleLabel.text = self.movie.title;
        self.DetailsLabel.text = self.movie.overview.length ? self.movie.overview : @"No Overview.";
        self.yearLabel.text = self.movie.releaseDate;
    }
}

#pragma mark - yt delegate

- (void)playerView:(nonnull YTPlayerView *)playerView receivedError:(YTPlayerError)error {
}

#pragma mark - CollectionView Datasource and delegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.movie.previews.count ?: 1;
}

-(__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"youtubeCollectionCell" forIndexPath:indexPath];
    if (!self.movie.previews.count) {
        return cell;
    }
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGSize videoSize = CGSizeMake(screenRect.size.width/2, (screenRect.size.width/2)/9*6);
    
    TMDBMoviePreview *preview = [self.movie.previews objectAtIndex:indexPath.row];
    
    if (preview.site == siteTypeYoutube) {
        YTPlayerView *view = [[YTPlayerView alloc] initWithFrame:CGRectMake(0, 0, videoSize.width, videoSize.height)];
        view.delegate = self;
        view.center = CGPointMake(cell.bounds.size.width/2, cell.bounds.size.height/2);
        [view loadWithVideoId:preview.key];
        [cell addSubview:view];
    } else if (preview.site == siteTypeVimeo) {
        WKWebView *webview = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, videoSize.width, videoSize.height)];
        webview.center = CGPointMake(cell.bounds.size.width/2, cell.bounds.size.height/2);
        webview.backgroundColor = [UIColor clearColor];
        [cell addSubview:webview];
        NSString *html = [NSString stringWithFormat:@"<html><head><style type=\"text/css\">body {background-color: transparent;color: black;}</style><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=yes\"/></head><body style=\"margin:0\"><div><iframe src=\"https://player.vimeo.com/video/%@\" width=\"%i\" height=\"%i\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div></body></html>",preview.key, (int)videoSize.width,(int)videoSize.height];
        webview.contentMode = UIViewContentModeScaleAspectFit;
        NSURL *url = [NSURL URLWithString:@"https://"];
        [webview loadHTMLString:html baseURL:url];
    } else {
    }
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGSize videoSize = CGSizeMake(screenRect.size.width/2, (screenRect.size.width/2)/9*6);
    return videoSize;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 10.0f;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
