//
//  MovieHomeViewController.m
//  testApp
//
//  Created by Edmund Heng on 1/12/20.
//

#import "MovieHomeViewController.h"
#import "TMDBCollectionViewController.h"
#import "AppDelegate.h"
#import "UILoadingView.h"
#import "TMDBHelper.h"
#import "TMDBViewPagerController.h"



@interface MovieHomeViewController ()
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UICollectionView *genreCollectionView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *containerViewForDetails;

@property (weak, nonatomic) IBOutlet UICollectionViewFlowLayout *genreLayout;
@property (weak, nonatomic) NSArray *genres;
@property (strong, nonatomic) NSMutableDictionary *loadingViews;
@property (assign, nonatomic) int currentGenre;
@property (assign, nonatomic) int currentPage;
@property (strong, nonatomic) genreCollectionCell *lastSelectedGenreCell;
@property (strong, nonatomic) UITapGestureRecognizer *tapGesture;
@property (nonatomic, strong) NSIndexPath *chosenItem;
@property (nonatomic, strong) TMDBViewPagerController *details;
@property (nonatomic, strong) TMDBCollectionViewController *collection;

@property (nonatomic, assign) BOOL viewMode;

@end

@implementation MovieHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.loadingViews = [NSMutableDictionary new];
    self.currentPage = 1;
    self.viewMode = YES;
    
    [self setupSegmentedControls];
    [self setupCollectionView];
    [self setUpSearchField];
    [self setupContainers];
    [self retrieveInitialData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(genresUpdated:)
                                                 name:genresUpdatedNotification
                                               object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Controller Methods

-(void) startSearch:(NSString *)searchString {
    self.searchBar.text = searchString;
    [self searchBar:self.searchBar textDidChange:searchString];
}

- (void) startSearchForGenre:(int)genreID {
    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        NSDictionary *dict = (NSDictionary *)evaluatedObject;
        int identifier = [[dict objectForKey:@"id"] intValue];
        return genreID == identifier;
    }];
    NSArray *filtered = [self.genres filteredArrayUsingPredicate:predicate];
    if (filtered.count) {
        NSDictionary *d = [filtered firstObject];
        int index = [self.genres indexOfObject:d];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        genreCollectionCell * cell = (genreCollectionCell *)[self collectionView:self.genreCollectionView cellForItemAtIndexPath:indexPath];
        [cell updateState:YES];
        [self.view layoutIfNeeded];
        [self searchForGenre:cell];
    }
}

-(void) dismissKeyboard {
    [self.view endEditing:YES];
}

- (void) switchContainer:(BOOL)details {
    self.containerViewForDetails.hidden = details;
    self.containerView.hidden = !details;
    
    if (details) {
        self.details.appeared = YES;
        self.collection.appeared = NO;
        [self.details updateDataAndView:self.chosenItem];
    } else {
        self.collection.appeared = YES;
        self.details.appeared = NO;
        [self.collection updateDataAndView];
    }
    self.segmentedControl.selectedSegmentIndex = (int)details;
}

#pragma mark - Search Bar Delegate

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (![self.loadingViews objectForKey:@"resultView"]) {
        UILoadingView *loading = [UILoadingView new];
        loading.frame = self.containerView.frame;
        [self.view addSubview:loading];
        [loading layoutSubviews];
        [self.loadingViews setObject:loading forKey:@"resultView"];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviesUpdated:)
                                                     name:movieListUpdatedNotification
                                                   object:nil];
    }
    
    self.details.currentIndex = -1;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate startSearch:searchText];
}

#pragma mark - Genre Collection Delegate

-(void)searchForGenre:(genreCollectionCell *)genreCell {
    if (![self.loadingViews objectForKey:@"genreSearch"]) {
        UILoadingView *loading = [UILoadingView new];
        loading.frame = self.containerView.frame;
        [self.view addSubview:loading];
        [loading layoutSubviews];
        [self.loadingViews setObject:loading forKey:@"genreSearch"];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviesUpdated:)
                                                     name:movieListUpdatedNotification
                                                   object:nil];
    }
    
    
    [self.lastSelectedGenreCell updateState:NO];
    genreCollectionCell *lastCell = self.lastSelectedGenreCell;
    self.lastSelectedGenreCell = genreCell == self.lastSelectedGenreCell ? nil : genreCell;
    self.currentGenre = self.lastSelectedGenreCell ? (int)self.lastSelectedGenreCell.tag : -1;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate clearSearchMode];
    self.searchBar.text = nil;
    self.details.currentIndex = -1;
    
    [TMDBHelper searchForGenre:self.currentGenre handler:^(id item, NSError * _Nullable error) {
        
        if (item) {
            //reset data
            appDelegate.movieList = [NSMutableDictionary new];
            self.currentPage = 1;
            [appDelegate updateMovieList:(NSDictionary *)item currentPage:self.currentPage andGenre:self.currentGenre];
        } else {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Unexpected error occurred retrieving data, please try again." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
                if (self.lastSelectedGenreCell) {
                    [genreCell updateState:NO];
                }
                
                [lastCell updateState:YES];
                self.currentGenre = (int)lastCell.tag;
            }];

            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
}

#pragma mark - CollectionView Datasource and delegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.genres.count;
}

-(__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    genreCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"genreCollectionCell" forIndexPath:indexPath];
    cell.delegate = self;
    NSDictionary *genre = [self.genres objectAtIndex:indexPath.row];
    [cell updateButton:genre];
    BOOL selected = self.currentGenre == [[genre objectForKey:@"id"] intValue];
    [cell updateState:selected];
    if (selected) {
        self.lastSelectedGenreCell = cell;
    }
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *genre = [self.genres objectAtIndex:indexPath.row];
    int genreID = [[genre objectForKey:@"id"] intValue];
    CGSize expectedLabelSize = [[genre objectForKey:@"name"] sizeWithFont:[UIFont systemFontOfSize:15.0f]
                            constrainedToSize:CGSizeZero
                            lineBreakMode:NSLineBreakByWordWrapping];
    if (self.currentGenre == genreID) {
        CGSize expectedLabelSize = [[genre objectForKey:@"name"] sizeWithFont:[UIFont boldSystemFontOfSize:15.0f]
                                constrainedToSize:CGSizeZero
                                lineBreakMode:NSLineBreakByWordWrapping];
    }
    
    CGSize itemSize = CGSizeMake(expectedLabelSize.width+15.0f, self.searchBar.bounds.size.height);
    return itemSize;
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 10.0f;
}

#pragma mark MovieTouchDelegate

-(void)tapForDetails:(NSIndexPath *)path {
    self.chosenItem = path;
    BOOL details = YES;
    [self switchContainer:details];
}

#pragma mark - Notifications

- (void) moviesUpdated:(NSNotification *)notification {
    if ([self.loadingViews objectForKey:@"resultView"]) {
        UILoadingView *loading = [self.loadingViews objectForKey:@"resultView"];
        [loading removeFromSuperview];
        [self.loadingViews removeObjectForKey:@"resultView"];
    }
    
    if ([self.loadingViews objectForKey:@"genreSearch"]) {
        UILoadingView *loading = [self.loadingViews objectForKey:@"genreSearch"];
        [loading removeFromSuperview];
        [self.loadingViews removeObjectForKey:@"genreSearch"];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:movieListUpdatedNotification object:nil];
}

- (void)genresUpdated:(NSNotification*)notification {
    self.genres = (NSArray *)notification.object;
    UILoadingView *view = [self.loadingViews objectForKey:@"collectionLoading"];
    if (view) {
        [view removeFromSuperview];
        [self.loadingViews removeObjectForKey:@"collectionLoading"];
    }
    [self.genreCollectionView reloadData];
}

#pragma mark - IBActions
- (IBAction)segmentedControlChanged:(UISegmentedControl *)sender {
    BOOL details = sender.selectedSegmentIndex == 1;
    
    if (details)
        self.chosenItem = nil;
    [self switchContainer:details];
    
    self.searchBar.text = @"";
//    [self startSearch:self.searchBar.text];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate clearSearchMode];
}

#pragma mark - Initial Setup

- (void) retrieveInitialData {
    [TMDBHelper searchForGenre:self.currentGenre handler:^(id item, NSError * _Nullable error) {
        
        if (item) {
            //reset data
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            appDelegate.movieList = [NSMutableDictionary new];
            self.currentPage = 1;
            
            [appDelegate updateMovieList:(NSDictionary *)item currentPage:self.currentPage andGenre:self.currentGenre];
        } else {
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"Unexpected error occurred retrieving data, please try again." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction * action) {
            }];

            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
    }];
}

- (void) setupContainers {
    [self switchContainer:NO];
}

- (void) setupCollectionView {
    self.currentGenre = -1;
    self.genreCollectionView.contentInset =UIEdgeInsetsMake(0, 15.0f, 0, 15.0f);
    UILoadingView *loading = [UILoadingView new];
    loading.frame = self.genreCollectionView.frame;
    [self.view addSubview:loading];
    [loading layoutSubviews];
    [self.loadingViews setObject:loading forKey:@"collectionLoading"];
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.genres = delegate.genreList;
    if(self.genres && self.genres.count) {
        [loading removeFromSuperview];
    }
}

- (void) setupSegmentedControls {
    self.segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
    self.segmentedControl.selectedSegmentIndex = 0;
}

- (void) setUpSearchField {
    self.tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    self.tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:self.tapGesture];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"detailsSegue"]) {
        self.details = (TMDBViewPagerController*)[segue destinationViewController];
    } else if([segue.identifier isEqualToString:@"listSegue"]) {
        TMDBCollectionViewController *controller = [segue destinationViewController];
        controller.delegate = self;
        self.collection = controller;
    }
//    segue.destinationViewController;
}


@end
